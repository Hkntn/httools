function ConvertFrom-HTSerializedJson {
	[cmdletbinding()]
	Param
	(
		[Parameter(Mandatory = $true,Position=1)]
		[string]$File
	)
	Begin
	{
		$File = Get-Item -LiteralPath $File
	}

	Process
	{
		Add-Type -Assembly System.Web.Extensions
		$text = [IO.File]::ReadAllText($File)
		$parser = New-Object Web.Script.Serialization.JavaScriptSerializer
		$parser.MaxJsonLength = $text.length
		$parser.DeserializeObject($text)
	}
}

function ConvertTo-HTSerializedJson {
	[cmdletbinding()]
	Param
	(
		[Parameter(Mandatory = $true,Position=1)]
		[System.Object]$Object,
		[Parameter(Mandatory = $true,Position=2)]
		[string]$File
	)
	Process
	{
		ConvertTo-Json $Object | Out-File -LiteralPath $File -Encoding utf8
	}
}

function Invoke-HTPlatformMessage {
	[CmdletBinding()]
	param (
		[string]$RawMessage,
		[string]$ToDirectComponent,
		[int]$Count=1
	)
	begin {
		if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
		{
		$certCallback = '
			using System;
			using System.Net;
			using System.Net.Security;
			using System.Security.Cryptography.X509Certificates;
			public class ServerCertificateValidationCallback
			{
				public static void Ignore()
				{
					if(ServicePointManager.ServerCertificateValidationCallback ==null)
					{
						ServicePointManager.ServerCertificateValidationCallback += 
							delegate
							(
								Object obj, 
								X509Certificate certificate, 
								X509Chain chain, 
								SslPolicyErrors errors
							)
							{
								return true;
							};
					}
				}
			}
		'
		Add-Type $certCallback
		 }
		[ServerCertificateValidationCallback]::Ignore()
	}
	process {
		$posturi = 'https://localhost:8001'

		if($ToDirectComponent -ne '')
		{
			$posturi = "https://localhost:8003/$ToDirectComponent/1"
		}

		$param = @{
			'Uri' = $posturi;
			'Method' = 'POST';
			'Headers' = @{'Content-Type'='application/json'};
			'Body' = $RawMessage
			}

			1..$Count | ForEach-Object {
				$cmd_result = Invoke-WebRequest @param
				Write-Information "Response:`n $($cmd_result.RawContent)"
			}

            Write-Information "Post URL: $posturi"  -ForegroundColor Yellow
            Write-Information "Count: $Count" -ForegroundColor Yellow
	}
}

function New-HTTempFolder {
	[CmdletBinding()]
	param
	(
		[parameter(Position = 0)]
		[string]$FolderName
	)
	process
	{
		if ($FolderName -eq '')
		{
			return
		}
		$root_folder = Join-Path $env:TEMP 'HTToolsTemp'

		if (-not (Test-Path $root_folder))
		{
			New-Item $root_folder -ItemType Directory | Out-Null
		}

		$root_folder = Resolve-Path $root_folder

		$folder = Join-Path $root_folder $FolderName

		if (-not (Test-Path $folder))
		{
			$ret = New-Item $folder -ItemType Directory
		}
		else
		{
			$ret = Get-Item $folder
		}
		return $ret
	}
}

function Remove-HTTempFolder {
	[CmdletBinding()]
	param
	(
		[parameter(Position = 0)]
		[string]$FolderName,
		[switch]$FlushTemp
	)
	process
	{
		$root_folder = Join-Path $env:TEMP 'HTToolsTemp'

		if (-not (Test-Path $root_folder))
		{
			Write-Error "Root Temp Folder not initialized..,"
			return
		}

		if ($FlushTemp)
		{
			Remove-Item $root_folder -Confirm:$false -Recurse

			Write-Error "All data in temp folder deleted..."
			return
		}
		if ($FolderName -eq '')
		{
			return
		}

		$FolderName = Join-Path $root_folder $FolderName
		if (-not (Test-Path $FolderName))
		{
			Write-Error "$FolderName not found... "
			return
		}
		Remove-Item $FolderName -Recurse -Confirm:$false
	}

}

function Write-HTElasticDocument {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingConvertToSecureStringWithPlainText", "", Justification="Function is just for debugging purposes")]
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true)]
		[string]$ElasticUri,

		[Parameter(Mandatory=$true)]
		[string]$Index,

		[Parameter(Mandatory=$true)]
		[string]$Type,

		[Parameter(Mandatory=$true)]
		[string]$Document,

		[Parameter(Mandatory=$true)]
		[string]$ElasticUser,

		[Parameter(Mandatory=$true)]
		[string]$ElasticUserp
	)

	process {
        $es_username = $ElasticUser
        $es_password = $ElasticUserp
        $es_uri =  "$ElasticUri/$Index/$Type"

        $es_securecred = ConvertTo-SecureString $es_password -AsPlainText -Force

        $es_creds = New-Object -TypeName 'System.Management.Automation.PSCredential' -ArgumentList $es_username, $es_securecred

		
        $param = @{
	        'Uri' = $es_uri;
	        'Method' = 'POST';
            'Credential' = $es_creds;
            'Body' = $Document
	        }

        $cmd_result = Invoke-WebRequest @param

        Write-Output $cmd_result.Content

	}
}

function Get-HTLicenseDays {
	[CmdletBinding()]
	param (
		[Parameter(Position=0, Mandatory=$true)]
		[string]
		$EndDate
	)
	process {
		
		try {
			$end = [System.DateTime]::ParseExact($EndDate, 'dd/MM/yyyy', [System.Globalization.CultureInfo]::InvariantCulture)
			Write-Host "End date is =  $end"
		}
		catch {
			Write-Host "Cannot Parse date $EndDate - with parser dd/mm/yyyy"
		}

		$timespan = $end - $(Get-Date)
		Write-Output $timespan.Days
	}
}

function Show-HTComponentStoreInfo {
	[CmdletBinding()]
	param (
		
	)
	process {
		Invoke-Expression -Command 'Dism.exe /Online /Cleanup-Image /AnalyzeComponentStore'
	}
}

function Start-HTComponentCleanup {
	[CmdletBinding()]
	param (
		
	)
	process {
		Invoke-Expression -Command 'Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase'
	}

}

function Convertfrom-HTJsonString {
	[CmdletBinding()]
	param (
		# String input of JSON
		[Parameter(Mandatory=$true)]
		[string]
		$JsonString
	)
	process {
		$hashtbl = [HTdotnet.Tools.Prototype]::DeserializeObject($JsonString)
		return [pscustomobject]$hashtbl
	}
}

function Set-FileEncodingToUTF8 {
    [CmdletBinding()]
    param (
        # String original File Path to convert
        [Parameter(Mandatory=$true)]
        [string]
        $OriginalFilePath,

        # Leave the original file with extension .orig
        [bool]
        $RemoveOriginalFile = $true,

        # Leave original files in tempFolder
        [bool]
        $FlushTemp = $true
    )
    process {
        $tempFolderName = "FileEncodingToUTF8"
        $origFilePathInfo = Resolve-Path  $OriginalFilePath -ErrorAction Stop
        $tempFolder = New-HTTempFolder -FolderName $tempFolderName
        $tempFileName = $origFilePathInfo.Path.Split('\')[-2] + "--" + $origFilePathInfo.Path.Split('\')[-1]
        Copy-Item $origFilePathInfo.Path "$(Join-Path $tempFolder.FullName $tempFileName)"

        $origFileContent = [System.IO.File]::ReadAllText($origFilePathInfo.Path)

        if($RemoveOriginalFile)
        {
            Remove-Item $origFilePathInfo.Path -Force
        }else {
            Rename-Item -LiteralPath $origFilePathInfo.Path -NewName $($origFilePathInfo.Path + ".orig")
        }

        $origFileContent | Out-File $origFilePathInfo.Path -Encoding utf8

        if($FlushTemp)
        {
            Remove-HTTempFolder -FolderName $tempFolderName
        }
    }
}

function Format-HTByteSize {
	[CmdletBinding()]
	param (
		# Integer byte input
		[Parameter(Mandatory=$true)]
		[Int64]
		$size
	)
	process {
		If ($size -gt 1TB) {[string]::Format("{0:0.00} TB", $size / 1TB)}
		ElseIf ($size -gt 1GB) {[string]::Format("{0:0.00} GB", $size / 1GB)}
		ElseIf ($size -gt 1MB) {[string]::Format("{0:0.00} MB", $size / 1MB)}
		ElseIf ($size -gt 1KB) {[string]::Format("{0:0.00} kB", $size / 1KB)}
		ElseIf ($size -gt 0) {[string]::Format("{0:0} B", $size)}
		Else {""}
	}
}

function Get-HTProcessFileHandles {
	[CmdletBinding()]
	param (

	)
	
	process {
		$handleExePath = Join-Path $PSScriptRoot "\3rd\handle.exe"
		$output = & $handleExePath /accepteula


		$cmd_result = ""
		foreach ($line in $output) {
			$cmd_result += "$line`n"
		}
		$sectionPattern = "(?:\n\n[-]+\n\n)(?<process_name>\S+) pid: (?<process_pid>\d+)\s+(?<user>[^\n]+)(?<section>.*?)(?=\n\n--)"
		$sectionRegex = New-Object -TypeName System.Text.RegularExpressions.Regex -ArgumentList ($sectionPattern,[System.Text.RegularExpressions.RegexOptions]::Singleline)

		$entryPattern = "(?<handle_hex>\S+):\s+File\s+\((?<mode>\S+)\)\s+(?<file>[^\n]+)"
		$entryRegex = New-Object -TypeName System.Text.RegularExpressions.Regex -ArgumentList ($entryPattern,[System.Text.RegularExpressions.RegexOptions]::Singleline)
	
		$sectionMatchs = $sectionRegex.Matches($cmd_result)
		$result =  @()
		foreach ($m in $sectionMatchs)
		{
			$proc = [PSCustomObject]@{
				ProcessName = $m.Groups['process_name'].Value
				ProcessId = $m.Groups['process_pid'].Value
				Username = $m.Groups['user'].Value
				FileHandles = @()
			}

			$entries = $entryRegex.Matches($m.Groups['section'].Value)

			foreach ($entry in $entries)
			{
				$proc.FileHandles += [PSCustomObject]@{
					FilePath = $entry.Groups['file'].Value
					Mode = $entry.Groups['mode'].Value
					Handle = $entry.Groups['handle_hex'].Value
				}
			}

			$result += $proc
		}

		return $result

	}
	
}

function Get-HTFileHandles{
	[CmdletBinding()]
	param (
		[string]
		$FileNamePattern = ""
	)
	
	process {
		$procHandles = Get-HTProcessFileHandles
		$FileNamePattern = $FileNamePattern.ToLowerInvariant()
		$result = @()

		foreach ($proc in $procHandles) {
			foreach ($filehandle in $proc.FileHandles) {
				$filePath = $filehandle.FilePath.ToLowerInvariant()
				if($filePath.Contains($FileNamePattern))
				{
					$result += [PSCustomObject]@{
						FilePath = $filehandle.FilePath
						Username = $proc.Username
						ProcessName = $proc.ProcessName
						ProcessId = $proc.ProcessId
						Mode = $filehandle.Mode
					}

				}
			}
		}
		return $result
	}
	
}

function Read-HTIniFile {
    [CmdletBinding()]
    param (
        # File Path
        [Parameter(Mandatory=$true)]
        [string]
        $FilePath
    )
    
    process {
        if(!(Test-Path -Path $FilePath))
        {
            Write-Error "File not found at $FilePath."
            return
        }
        $lines = Get-Content $FilePath
        $sectionRegex = [Regex]"^\[(?<section>.+)\]\s*$"
        $entryRegex = [Regex]"^(?<ekey>[^\s=]+)\s*=\s*(?<evalue>.*)$"
        $ini = [PSCustomObject]@{
            ROOTSECTIONOFINI = @{}
        }
        $section = 'ROOTSECTIONOFINI'
        $i = 0
        foreach ($line in $lines) {
            $i++
            # if empty line or comment continue to next line
            if($line.StartsWith('#') -or $line -match '^\s+$' -or $line.StartsWith(';')){continue}
            # if line is starting with a section openining try to match and add section
            if($line.StartsWith('['))
            {
                # Parse Section and add to iniCollection
                $match = $sectionRegex.Match($line)
                if($match.Success)
                {
                    $section = $match.Groups['section'].Value

                    try {
                        Add-Member -InputObject $ini -MemberType NoteProperty -Name $section -Value @{} -ErrorAction Stop
                    }
                    catch {
                        Write-Error "Duplicate section detected --> [$section] at line $i "
                        throw
                    }
                }
                # If not match or section added continue to next line
                continue
            }

            #Line is not a comment or section, it should be an entry
            #Try to parse the entry and add to the current section.
            #If no section provided entry will be added to the root section

            $entryMatch = $entryRegex.Match($line)
            if($entryMatch.Success)
            {
                $entryKey = $entryMatch.Groups['ekey'].Value
                $entryValue = $entryMatch.Groups['evalue'].Value
                try {
                    $ini.$section.Add($entryKey,$entryValue) 
                }
                catch {
                    Write-Error "Duplicate key detected at section [$section] --> $entryKey at line $i "
                    throw
                }
                # $ini.$section.Add($entryKey,$entryValue) 
            }


        }
        return $ini
    }
}

function Write-HTIniFile {
    [CmdletBinding()]
    param (
        # Ini Object
        [Parameter(Mandatory=$true)]
        [PSCustomObject]
        $IniObject,

        # Destination File to Write
        [Parameter(Mandatory=$true)]
        [string]
        $DestinationFilePath
    )
    process {
        $output = @()
        $sections = Get-Member -InputObject $IniObject -MemberType NoteProperty
        # Add root items first
        foreach ($ritem in $IniObject.ROOTSECTIONOFINI.GetEnumerator()) {
            $output += $ritem.Key+'='+$ritem.Value
        }

        foreach ($section in $sections) {
            # Root items added skip them..
            if($section.Name -eq 'ROOTSECTIONOFINI') {continue}
            if($section.Definition.StartsWith('hashtable'))
            {
                $output += "`n["+$section.Name+"]"
                foreach ($item in $IniObject.$($section.Name).GetEnumerator()) {
                    $output += $item.Key+'='+$item.Value
                }
            }
        }

        $output | Out-File -FilePath $DestinationFilePath -Encoding utf8
        
    }
}