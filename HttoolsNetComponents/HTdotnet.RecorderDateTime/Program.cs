﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace HTdotnet.RecorderDateTime
{
    public static class Program
    {
        private const string DefaultConfig =
@"---
Name: Test1
Date: 2019-05-07 00:00:04,500
Format: yyyy'-'MM'-'dd' 'HH':'mm':'ss','fff
Enabled: y
===
---
Name: Test2
Date: 2019-05-07 00:00:04,500
Format: yyyy'-'MM'-'dd' 'HH':'mm':'ss'
Enabled: n
===
---
Name: Test3
Date: 2019-06-12 07:42:04
Format: yyyy'-'MM'-'dd' 'HH':'mm':'ss'
Enabled: y
===";

        public static void Main(string[] args)
        {
            var dataFile = args != null && args.Length == 1 ? args[0] : "config.txt";
            do
            {
                var tests = LoadTestsFromFile(dataFile);
                RunTests(tests);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Write 'e' to exit or press enter to retry:");
                Console.ResetColor();
            } while (Console.ReadLine() != "e");
        }

        public static List<FormatTest> LoadTestsFromFile(string filename)
        {
            var list = new List<FormatTest>();
            if (!File.Exists(filename))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Cannot find config file {filename}.\nCreating default configuration config.txt");
                Console.ResetColor();
                filename = "config.txt";
                File.WriteAllText(filename, DefaultConfig);
                Environment.Exit(0);
                
            }

            var dataFileContent = File.ReadAllText(filename);
            const string pattern = "(?<Config>---.*?===)";
            var configMatch = Regex.Matches(dataFileContent, pattern, RegexOptions.Singleline);


            foreach (Match match in configMatch)
            {
                const string configPattern = "Name:\\s+(?<Name>[^\\n\\r]+)[\\r\\n]+Date:\\s+(?<Date>[^\\n\\r]+)[\\r\\n]+Format:\\s(?<Format>[^\\n\\r]+)[\\r\\n]+Enabled:\\s+(?<Enabled>[^\\n\\r]+)";
                var config = Regex.Match(match.Groups["Config"].Value, configPattern, RegexOptions.Singleline);
                if (config.Success)
                {
                    list.Add(new FormatTest()
                    {
                        Date = config.Groups["Date"].Value,
                        Format = config.Groups["Format"].Value,
                        Name = config.Groups["Name"].Value,
                        Enabled = config.Groups["Enabled"].Value
                    });
                }
            }

            return list;
        }

        public static void RunTests(IEnumerable<FormatTest> tests)
        {
            Console.Clear();
            foreach (var test in tests)
            {
                if (test.Enabled != "y") continue;

                try
                {
                    var parsed = DateTime.ParseExact(test.Date, test.Format, CultureInfo.InvariantCulture);
                    Console.WriteLine($"Date is parsed for config{test.Name}. {test.Date} => {parsed}");
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Unable to parse {test.Name} Exception:{e.Message}");
                    Console.ResetColor();
                }
            }
        }


    }

    public class FormatTest
    {
        public string Date { get; set; }
        public string Format { get; set; }
        public string Name { get; set; }
        public string Enabled { get; set; }
    }
}
