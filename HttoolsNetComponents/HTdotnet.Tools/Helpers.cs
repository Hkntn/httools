﻿using System.Collections;
using System.IO;
using Newtonsoft.Json;

namespace HTdotnet.Tools
{
    public static class Helpers
    {
        public static Hashtable DeserializeObject(string serializedString)
        {
            return JsonConvert.DeserializeObject<Hashtable>(serializedString);
        }

        public static double GetLinesInFile(string filePath)
        {
            var fileHandle = File.OpenRead(filePath);

            var streamReader = new StreamReader(fileHandle);

            double count = 0;

            while (streamReader.ReadLine() != null)
            {
                count++;
            }

            return count;
        }
    }
}