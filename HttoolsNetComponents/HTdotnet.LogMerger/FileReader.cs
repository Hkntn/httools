﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using NLog;

namespace HTdotnet.LogMerger
{
    public class FileReader
    {
        private FileState _state;
        private FileStream _fsStream;
        private readonly StringBuilder _stringBuffer = new StringBuilder();
        public readonly string FilePath;
        private readonly string ConfigName;
        private readonly string StateFileName;
        private readonly Regex RecordRegex;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public FileReader(string filePath, string configName,string regex)
        {
            FilePath = filePath;
            ConfigName = configName;
            StateFileName = Path.Combine(Helpers.StateFolder,$"{Helpers.SanitizePath(FilePath)}.json");
            RecordRegex = new Regex(regex, RegexOptions.Multiline | RegexOptions.Singleline);
            LoadState();
        }

        private void LoadState()
        {
            try
            {
                if (File.Exists(StateFileName))
                {
                    var stateStr = File.ReadAllText(StateFileName);
                    _state = JsonConvert.DeserializeObject<FileState>(stateStr);
                }
                if(_state == null)
                {
                _state = new FileState(FilePath, ConfigName);
                }

                _fsStream = new FileStream(FilePath,FileMode.Open,FileAccess.Read,FileShare.Delete|FileShare.ReadWrite);
                if (_state.LastProcessedOffset < _fsStream.Length)
                {
                    _fsStream.Seek(_state.LastProcessedOffset, SeekOrigin.Begin);
                    return;
                }

                _state.LastProcessedOffset = 0;
                _fsStream.Seek(0, SeekOrigin.Begin);
                SaveState();
            }
            catch (Exception e)
            {
                Logger.Error($"Cannot load or initialize state.\n{e}");
            }
        }

        private void SaveState()
        {
 

            try
            {
                var saveOffset = _state.LastProcessedOffset -
                                 Encoding.UTF8.GetBytes(_stringBuffer.ToString()).Length;
                var state = _state.CopyWithCustomOffset(saveOffset);
                var serialized = JsonConvert.SerializeObject(state);
                File.WriteAllText(StateFileName, serialized);
            }
            catch (Exception e)
            {
                Logger.Error($"Cannot save state of the reader.\n{e}");
            }
        }

        public List<string> Read()
        {
            var buffer = new byte[Helpers.ReaderBuffer];

            if (_state.LastProcessedOffset < _fsStream.Length)
            {
                while (true)
                {
                    var readBytes = _fsStream.Read(buffer, 0, Helpers.ReaderBuffer);
                    if(readBytes<=0) break;
                    _stringBuffer.Append(Encoding.UTF8.GetString(buffer.Take(readBytes).ToArray()));
                }
                
                _state.LastProcessedOffset = _fsStream.Position;
                return ParseStringBuffer();
            }

            if (_state.LastProcessedOffset > _fsStream.Length)
            {
                //overwritten
                _state.LastProcessedOffset = 0;
                SaveState();
                return null;
            }


            

            // No Changes
            return null;
        }

        private List<string> ParseStringBuffer()
        {
            var results = new List<string>();
            var raw = _stringBuffer.ToString();
            var matches = RecordRegex.Matches(raw);
            var strPosition = 0;
            foreach (Match match in matches)
            {
                var mValue = match.Groups["RECORD"].Value;
                var lastStrPosition = match.Index + mValue.Length;
                if (lastStrPosition > strPosition) strPosition = lastStrPosition;
                results.Add(mValue.TrimEnd('\n', '\r'));
            }

            _stringBuffer.Remove(0, strPosition);
            return results;
        }


        public void CommitLastOffset()
        {
            SaveState();
        }
    }
}