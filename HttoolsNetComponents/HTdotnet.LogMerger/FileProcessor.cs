﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using NLog;
using NLog.LayoutRenderers;

namespace HTdotnet.LogMerger
{
    public class FileProcessor
    {
        private readonly Logger _logger;
        private readonly Logger _outputWriter;
        private readonly Dictionary<string, FileReader> _readers;
        private readonly LogMergerConfig _config;
        public bool CanProcess = false;

        public FileProcessor(LogMergerConfig config)
        {
            _config = config;
            _logger = LogManager.GetLogger($"File Processor - {_config.Name}");
            _outputWriter = LogManager.GetLogger($"merged-{_config.Name}");
            _readers = new Dictionary<string, FileReader>();
            RefreshReaders();
        }


        public void Process()
        {
            RefreshReaders();
            _logger.Trace($"Tracked files refreshed, created readers for {_readers.Count} files..");
            foreach (var reader in _readers.Values)
            {
                var temp = reader.Read();
                if (temp == null) continue;

                foreach (var record in temp)
                {
                    _outputWriter.Info(record);
                }
                reader.CommitLastOffset();
            }
        }

        private void RefreshReaders()
        {
            var filesTracked = new List<string>();
            foreach (var path in _config.PathsList)
            {
                FileAttributes attributes;
                try
                {
                    attributes = File.GetAttributes(path);
                }
                catch (Exception e)
                {
                    _logger.Error($"Cannot get attributes of path: {path}\n{e}");
                    continue;
                }

                if (attributes == FileAttributes.Directory)
                {
                    if (_config.FindFilesRecursively)
                    {
                        filesTracked.AddRange(Directory.GetFiles(path, _config.FileNamePattern, SearchOption.AllDirectories).ToList());
                        continue;
                    }
                    filesTracked.AddRange(Directory.GetFiles(path, _config.FileNamePattern).ToList());
                    continue;
                }

                filesTracked.Add(path);
            }

            if (filesTracked.Count == 0)
            {
                CanProcess = false;
                return;
            }

            CanProcess = true;

            foreach (var file in filesTracked)
            {
                var sanitizedFileName = Helpers.SanitizePath(file);
                if(_readers.ContainsKey(sanitizedFileName)) continue;
                _readers.Add(sanitizedFileName,new FileReader(file,_config.Name,_config.RecordRegex));
            }

            var tempReaders = _readers.Keys.ToList();
            foreach (var fileReader in tempReaders)
            {
                if(filesTracked.Contains(_readers[fileReader].FilePath)) continue;
                _readers.Remove(fileReader);
            }
        }
    }
}