﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Timers;
using Newtonsoft.Json;
using NLog;

namespace HTdotnet.LogMerger
{
    public class LogMergerService
    {
        private const string ConfigFile = "config.json";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static LogMergerServiceConfig _config;
        private static List<LogMergerInstance> MergerInstances = new List<LogMergerInstance>();
        public LogMergerService()
        {

        }

        public void Start()
        {
            if (!File.Exists(ConfigFile))
            {
                Logger.Error($"Cannot find service config file {ConfigFile}, exiting");
                Environment.Exit(1);
            }
            Logger.Debug($"Found config file {ConfigFile}, trying to deserialize configuration");

            var configstr = File.ReadAllText(ConfigFile);

            try
            {
                _config = JsonConvert.DeserializeObject<LogMergerServiceConfig>(configstr);
            }
            catch (Exception e)
            {
                Logger.Error($"Cannot deserialize configuration:\n{e}");
                Environment.Exit(1);
            }

            foreach (var logMergerConfig in _config.Configs)
            {
                if (logMergerConfig.Enabled)
                {
                    MergerInstances.Add(new LogMergerInstance(logMergerConfig));
                }
            }

            Helpers.ReaderBuffer = _config.ReaderBuffer;

        }
        public void Stop() {}
    }
}