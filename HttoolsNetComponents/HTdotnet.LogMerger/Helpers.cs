﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HTdotnet.LogMerger
{
    public static class Helpers
    {
        private static string _stateFolder = "state";
        public static int ReaderBuffer;
        public static string StateFolder
        {
            get
            {
                if (!Directory.Exists(_stateFolder))
                {
                    Directory.CreateDirectory(_stateFolder);
                }
                return _stateFolder;
            }
            set => _stateFolder = value;
        }

        public static string SanitizePath(string path)
        {
            return Hash(path);
        }

        public static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }
    }
}