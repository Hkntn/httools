﻿namespace HTdotnet.LogMerger
{
    public class FileState
    {
        public string FilePath { get; set; }
        public string ConfigName { get; set; }
        public long LastProcessedOffset { get; set; }

        public FileState()
        {
            
        }

        public FileState(string filepath, string configName)
        {
            FilePath = filepath;
            ConfigName = configName;
            LastProcessedOffset = 0;
        }

        public FileState CopyWithCustomOffset(long offset)
        {
            return new FileState(){
                FilePath = FilePath,
                ConfigName = ConfigName,
                LastProcessedOffset = offset
                };
        }
    }
}