﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using Topshelf.Configurators;

namespace HTdotnet.LogMerger
{
    class Program
    {
        static void Main(string[] args)
        {
            var ex = HostFactory.Run(x =>
            {
                x.Service<LogMergerService>(s =>
                {
                    s.ConstructUsing(name => new LogMergerService());
                    s.WhenStarted(logMergerService => logMergerService.Start());
                    s.WhenStopped(logMergerService => logMergerService.Stop());
                });

                x.RunAsLocalSystem();
                x.SetDescription("HT Log File Merger");
                x.SetDisplayName("HT Log File Merger");
                x.SetServiceName("htdotnet.logFileMerger");
            });
        }
    }
}
