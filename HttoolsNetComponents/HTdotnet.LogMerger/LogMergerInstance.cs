﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using NLog;
using NLog.Fluent;

namespace HTdotnet.LogMerger
{
    public class LogMergerInstance
    {
        private Timer _timer;

        private readonly Logger Logger;

        private readonly LogMergerConfig _config;

        private FileProcessor _processor;

        
        public LogMergerInstance(LogMergerConfig config)
        {
            var st = Stopwatch.StartNew();
            _config = config;

            Logger = LogManager.GetLogger($"Merger Instance - {config.Name}");
            Init();
            st.Stop();
            Logger.Info($"Merger instance {_config.Name} is initialized in {st.ElapsedMilliseconds} milliseconds.");

        }

        private void Init()
        {
            _processor = new FileProcessor(_config);
            _timer = new Timer(TimerOnElapsed, null,0,_config.RefreshInterval);
        }



        private void TimerOnElapsed(object sender)
        {
            try
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                if (_processor.CanProcess)
                {
                    Logger.Trace("Starting to process...");
                    _processor.Process();
                    _timer.Change(_config.RefreshInterval, _config.RefreshInterval);
                    return;
                }
                Logger.Error("No files found..");
                _timer.Change(_config.RefreshInterval, _config.RefreshInterval);
            }
            catch (Exception e)
            {
                Logger.Error($"Error on timer cycle..\n {e}");
                throw;
            }
        }
    }
}