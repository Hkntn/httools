﻿using System.Collections.Generic;

namespace HTdotnet.LogMerger
{
    public class LogMergerServiceConfig
    {
        public List<LogMergerConfig> Configs { get; set; }
        public int ReaderBuffer { get; set; }

        public LogMergerServiceConfig()
        {
            Configs = new List<LogMergerConfig>();
        }
    }
}