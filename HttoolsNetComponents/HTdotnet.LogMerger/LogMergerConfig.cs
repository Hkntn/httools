﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;

namespace HTdotnet.LogMerger
{
    public class LogMergerConfig
    {
        public List<string> PathsList { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string FileNamePattern { get; set; }
        public int RefreshInterval { get; set; }
        public bool FindFilesRecursively { get; set; }
        public string OutputFile { get; set; }
        public string RecordRegex { get; set; }

        public LogMergerConfig()
        {
            PathsList = new List<string>();
        }
    }
}